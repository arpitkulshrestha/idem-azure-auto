import uuid

import pytest


@pytest.mark.skip(
    reason="Test will be disabled until module is refactored with new design changes to support caching"
)
@pytest.mark.asyncio
async def test_network_security_group(hub, ctx, resource_group_fixture):
    """
    This test provisions a network security group, describes network security group and deletes
     the provisioned network security group.
    """
    # Create network security group
    resource_group_name = resource_group_fixture.get("name")
    network_security_group_name = "idem-test-security-group-" + str(uuid.uuid4())
    sg_parameters = {
        "location": "eastus",
    }
    sg_ret = await hub.states.azure.virtual_networks.network_security_groups.present(
        ctx,
        name=network_security_group_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        parameters=sg_parameters,
    )
    assert sg_ret["result"], sg_ret["comment"]
    assert not sg_ret["changes"].get("old") and sg_ret["changes"]["new"]

    sg_wait_ret = await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
    hub.tool.azure.resource.check_response_payload(sg_parameters, sg_wait_ret["ret"])

    # Describe network security group
    describe_ret = (
        await hub.states.azure.virtual_networks.network_security_groups.describe(ctx)
    )
    assert sg_wait_ret["ret"].get("id") in describe_ret

    # Delete network security group
    sg_del_ret = await hub.states.azure.virtual_networks.network_security_groups.absent(
        ctx,
        name=network_security_group_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
    )
    assert sg_del_ret["result"], sg_del_ret["comment"]
    assert sg_del_ret["changes"]["old"] and not sg_del_ret["changes"].get("new")
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
